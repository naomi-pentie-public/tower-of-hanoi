(function () {

	var canvas = document.getElementById("myCanvas");
	var context = canvas.getContext("2d");

	// CONFIG

	var _spikeY = 200;
	var _spikeWidth = 13;
	var _spikeHeight = 150;
	var _diskHeight = 12;
	var _firstDiskWidth = 113;
	var _diskWidthFactor = 20;
	var _waitingTime = 3000; // in milisecunde

	var spikeA = {};
	spikeA.disks = [];
	spikeA.position = {};
	spikeA.width = _spikeWidth;
	spikeA.height = _spikeHeight;
	spikeA.position.x = 100;
	spikeA.position.y = 200;
	spikeA.position.disks = [{
			x: 50,
			y: 340
		},
		{
			x: 60,
			y: 325
		},
		{
			x: 70,
			y: 310
		}
	];

	var spikeB = {};
	spikeB.disks = [];
	spikeB.position = {};
	spikeB.width = _spikeWidth;
	spikeB.height = _spikeHeight;
	spikeB.position.x = 300;
	spikeB.position.y = 200;
	spikeB.position.disks = [{
			x: 250,
			y: 340
		},
		{
			x: 260,
			y: 325
		},
		{
			x: 270,
			y: 310
		}
	];

	var spikeC = {};
	spikeC.disks = [];
	spikeC.position = {};
	spikeC.width = _spikeWidth;
	spikeC.height = _spikeHeight;
	spikeC.position.x = 500;
	spikeC.position.y = 200;
	spikeC.position.disks = [{
			x: 450,
			y: 340
		},
		{
			x: 460,
			y: 325
		},
		{
			x: 470,
			y: 310
		}
	];

	var diskCount;
	var spikes = [];
	var callStack;
	var myTimer = null;

	function bindEvents() {
		var showDisksBtn = document.getElementsByClassName("show-disks")[0];
		var startGameBtn = document.getElementsByClassName("start-game")[0];

		showDisksBtn.addEventListener("click", showDisc);
		startGameBtn.addEventListener("click", start);
	}

	function initialize() {
		spikes = [spikeA, spikeB, spikeC];
		drawSpikes();
		callStack = [];
	}

	function drawSpikes() {
		for (var i = 0; i < spikes.length; i++) {
			drawSpike(spikes[i].position.x, spikes[i].position.y, spikes[i].width, spikes[i].height);
		}
	}

	function drawSpike(x, y, width, height) {
		context.fillRect(x, y, width, height);
	}

	function getDisk(id) {
		return {
			id: id,
			color: "#" + Math.max(Math.floor(Math.random() * 999999) + 1, 100000),
			height: _diskHeight,
			width: (_firstDiskWidth - (id * _diskWidthFactor)),
			position: {}
		};
	}

	function getDiskCount() {
		return parseInt(document.getElementById("disk").value);
	}

	function showDisc() {
		clear();
		drawSpikes();
		diskCount = getDiskCount();

		for (var i = 0; i < diskCount; i++) {
			var disk = getDisk(i);
			disk.position = spikeA.position.disks[i];
			spikeA.disks.push(disk);
		}

		for (var i = 0; i < spikes.length; i++) {
			displayDisks(spikes[i]);
		}
	}

	function displaySpikesWithDisks() {
		drawSpikes();
		for (var i = 0; i < spikes.length; i++) {
			displayDisks(spikes[i]);
		}
	}

	function clear() {
		context.clearRect(0, 0, canvas.width, canvas.height);
	}

	function displayDisks(spike) {
		for (i = 0; i < spike.disks.length; i++) {
			context.fillStyle = spike.disks[i].color;
			context.fillRect(spike.disks[i].position.x, spike.disks[i].position.y, spike.disks[i].width, spike.disks[i].height);
		}
	}

	function displayOptimalMoves() {
		var div = document.getElementsByClassName('optimal-moves')[0];
		div.innerHTML = (Math.pow(2, diskCount) - 1);
	}

	function displayMove(moveInfo) {
		var movesList = document.getElementsByClassName("moves")[0];
		var move = document.createElement("li");
		var moveContent = document.createTextNode('Move disc ' + moveInfo.disc + ' from ' + moveInfo.from + ' to ' + moveInfo.to);
		move.appendChild(moveContent);
		movesList.appendChild(move);
	}

	function start() {
		diskCount = getDiskCount();
		callStack = [];

		if (spikeA.disks.length === 0) {
			alert("Da click pe show disk intai!");
			return;
		}
		displayOptimalMoves();
		// Miscarile initiale
		hanoi(diskCount, 0, 2, 1);
		moveDisk();

	}

	function hanoi(disc, from, temp, to) {

		if (disc > 0) {

			hanoi(disc - 1, from, to, temp);
			moveInfo = {disc: disc, from: from, to: to};
			displayMove(moveInfo);
			console.log('Move disc ' + disc + ' from ' + from + ' to ' + to);
			callStack.push([from, temp]);
			hanoi(disc - 1, to, temp, from);
		}
	}

	function moveDisk() {
		if (callStack.length == 0) return;

		var param = callStack.shift();

		var fromSpike = param[0];
		var toSpike = param[1];

		var disk = spikes[fromSpike].disks[spikes[fromSpike].disks.length-1];
		disk.position = spikes[toSpike].position.disks[disk.id];
		spikes[fromSpike].disks.pop();
		spikes[toSpike].disks.push(disk);

		myTimer = setInterval(animate, _waitingTime);
	}

	function animate() {

		clear();
		displaySpikesWithDisks();

		var lastSpike = spikes[spikes.length-1];
		if (lastSpike.disks.length === diskCount) {
			alert("Felicitari!");
			clearInterval(myTimer);
			return;
		}
		clearInterval(myTimer);
		moveDisk();
	}

	bindEvents();
	initialize();
})();