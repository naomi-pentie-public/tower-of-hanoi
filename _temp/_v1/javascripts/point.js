function Point(x, y) {
  this.x = x;
  this.y = y;
}

Point.prototype.subtract = function(point) {
  return new Point(this.x - point.x, this.y - point.y);
}

Point.prototype.distance_to = function(other) {
  return Math.sqrt(Math.pow(other.x - this.x, 2) + Math.pow(other.y - this.y, 2));
}

Point.prototype.toString = function() {
  return '(' + this.x + ', ' + this.y + ')';
}
