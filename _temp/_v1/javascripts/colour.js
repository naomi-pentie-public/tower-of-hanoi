function Colour(rgb) {
  this.rgb = rgb;
}

Colour.prototype.toString = function() {
  return 'rgb(' + this.rgb.join() + ')';
}

Colour.random = function() {
  return Colour.convert_hsv_to_rgb([random_int(0, 359),
                                    random_int(40, 80)/80,
                                    random_int(40, 80)/80]);
}

Colour.random_alternative = function() {
  var rgb = [random_int(0, 127), random_int(64, 192), random_int(128, 255)];
  shuffle(rgb);
  return new Colour(rgb);
}

Colour.convert_hsv_to_rgb = function(hsv) {
  var h = hsv[0], s = hsv[1], v = hsv[2];
  h = (h/60) % 6;
  var h_i = Math.floor(h);
  var f = h - h_i;
  var p = v*(1 - s);
  var q = v*(1 - f*s);
  var t = v*(1 - (1 - f)*s);

  switch(h_i) {
    case 0:
      var rgb = [v, t, p];
      break;
    case 1:
      var rgb = [q, v, p];
      break;
    case 2:
      var rgb = [p, v, t];
      break;
    case 3:
      var rgb = [p, q, v];
      break;
    case 4:
      var rgb = [t, p, v];
      break;
    case 5:
      var rgb = [v, p, q];
      break;
  }
  return new Colour(rgb.map(function(a) { return Math.round(a*256); }));
}