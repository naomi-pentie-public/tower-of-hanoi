var diskCount;
var moveCount = 0;

function init() {

  diskCount = parseInt(document.getElementById('no-of-disks').value);

  document.getElementById('start-new-game').addEventListener('click', function() {
    document.getElementById('introduction').style.display = 'none';
    diskCount = parseInt(document.getElementById('no-of-disks').value);
    document.getElementById('output').style.display = 'block';
    new Game(diskCount);
    document.getElementById("moves").textContent=moveCount;
  }, false);
}
window.addEventListener('load', init, false);
