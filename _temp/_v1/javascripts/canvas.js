function Canvas(canvas_id) {
  this.canvas_id = canvas_id;
  this.recreate();
}

Canvas.prototype.load_canvas = function() {
  this.canvas = document.getElementById(this.canvas_id);
  this.ctx = this.canvas.getContext('2d');
}

Canvas.prototype.set_width = function(width) {
  this.canvas.width = this.width = biggest(window.innerWidth, width);
}

Canvas.prototype.set_height = function(height) {
  this.canvas.height = this.height = height;
}

Canvas.prototype.clear = function() {
  this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
}

Canvas.prototype.recreate = function() {
  this.load_canvas();
  var canvas_prime = this.canvas.cloneNode(false);
  this.canvas.parentNode.replaceChild(canvas_prime, this.canvas);
  this.load_canvas();
}
