function Game(disks_count) {
  this.start_new(disks_count);
}

Game.prototype.start_new = function(disks_count) {

  var canvas = new Canvas('canvas');
  var tower_manager = new TowerManager(canvas, disks_count);
  var input_handler = new InputHandler(canvas.ctx, tower_manager);
  var game_state = new GameState(tower_manager, input_handler);
  var victory_celebrator = new VictoryCelebrator(input_handler);
  game_state.on_victory = function() { victory_celebrator.on_victory(); }

  tower_manager.draw();
  this.optimal_moves(disks_count);
}

Game.prototype.getOptimalMoves = function(disks_count) {
  return (Math.pow(2, disks_count) - 1);
};

Game.prototype.optimal_moves = function(disks_count) {
  document.getElementById("optimal-moves").textContent=this.getOptimalMoves(disks_count);
};
